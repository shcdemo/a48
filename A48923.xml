<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A48923">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Lamentation.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A48923 of text R43349 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing L279). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A48923</idno>
    <idno type="STC">Wing L279</idno>
    <idno type="STC">ESTC R43349</idno>
    <idno type="EEBO-CITATION">27248063</idno>
    <idno type="OCLC">ocm 27248063</idno>
    <idno type="VID">110053</idno>
    <idno type="PROQUESTGOID">2240891251</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A48923)</note>
    <note>Transcribed from: (Early English Books Online ; image set 110053)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1723:26)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Lamentation.</title>
     </titleStmt>
     <extent>1 sheet ([1] p.).</extent>
     <publicationStmt>
      <publisher>Printed for T.D.,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1679.</date>
     </publicationStmt>
     <notesStmt>
      <note>First line of text: Great Charles, we do lament thy Fate.</note>
      <note>Verses concerning the Popish Plot.</note>
      <note>Reproduction of original in Harvard University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Popish Plot, 1678.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>The Lamentation.</ep:title>
    <ep:author>[no entry]</ep:author>
    <ep:publicationYear>1679</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>358</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-12</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2008-01</date><label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2008-03</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-09</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A48923-t">
  <body xml:id="A48923-e0">
   <div type="poem" xml:id="A48923-e10">
    <pb facs="tcp:110053:1" rend="simple:additions" xml:id="A48923-001-a"/>
    <head xml:id="A48923-e20">
     <w lemma="the" pos="d" xml:id="A48923-001-a-0010">THE</w>
     <w lemma="lamentation" pos="n1" xml:id="A48923-001-a-0020">LAMENTATION</w>
     <pc unit="sentence" xml:id="A48923-001-a-0030">.</pc>
    </head>
    <lg xml:id="A48923-e30">
     <l xml:id="A48923-e40">
      <w lemma="great" pos="j" xml:id="A48923-001-a-0040">GReat</w>
      <w lemma="Charles" pos="nn1" rend="hi" xml:id="A48923-001-a-0050">Charles</w>
      <pc xml:id="A48923-001-a-0060">,</pc>
      <w lemma="we" pos="pns" xml:id="A48923-001-a-0070">we</w>
      <w lemma="do" pos="vvb" xml:id="A48923-001-a-0080">do</w>
      <w lemma="lament" pos="vvi" xml:id="A48923-001-a-0090">lament</w>
      <w lemma="thy" pos="po" xml:id="A48923-001-a-0100">thy</w>
      <w lemma="fate" pos="n1" xml:id="A48923-001-a-0110">Fate</w>
      <pc xml:id="A48923-001-a-0120">,</pc>
     </l>
     <l xml:id="A48923-e60">
      <w lemma="for" pos="acp" xml:id="A48923-001-a-0130">For</w>
      <w lemma="thou" pos="pns" xml:id="A48923-001-a-0140">thou</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-0150">the</w>
      <w lemma="object" pos="n1" xml:id="A48923-001-a-0160">Object</w>
      <w lemma="art" pos="n1" xml:id="A48923-001-a-0170">art</w>
      <w lemma="of" pos="acp" xml:id="A48923-001-a-0180">of</w>
      <w lemma="late" pos="j" xml:id="A48923-001-a-0190">late</w>
     </l>
     <l xml:id="A48923-e70">
      <w lemma="of" pos="acp" xml:id="A48923-001-a-0200">Of</w>
      <w lemma="popish" pos="j" xml:id="A48923-001-a-0210">Popish</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-0220">and</w>
      <w lemma="of" pos="acp" xml:id="A48923-001-a-0230">of</w>
      <w lemma="factious" pos="j" xml:id="A48923-001-a-0240">factious</w>
      <w lemma="hate" pos="n1" xml:id="A48923-001-a-0250">Hate</w>
      <pc unit="sentence" xml:id="A48923-001-a-0260">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e80">
     <l xml:id="A48923-e90">
      <w lemma="these" pos="d" xml:id="A48923-001-a-0270">These</w>
      <w lemma="wind" pos="n2" xml:id="A48923-001-a-0280">Winds</w>
      <w lemma="from" pos="acp" xml:id="A48923-001-a-0290">from</w>
      <w lemma="distant" pos="j" xml:id="A48923-001-a-0300">distant</w>
      <w lemma="quarter" pos="n2" xml:id="A48923-001-a-0310">Quarters</w>
      <w lemma="come" pos="vvb" xml:id="A48923-001-a-0320">come</w>
      <pc xml:id="A48923-001-a-0330">,</pc>
     </l>
     <l xml:id="A48923-e100">
      <w lemma="from" pos="acp" xml:id="A48923-001-a-0340">From</w>
      <w lemma="north" pos="n1" xml:id="A48923-001-a-0350">North</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-0360">and</w>
      <w lemma="south" pos="n1" xml:id="A48923-001-a-0370">South</w>
      <pc xml:id="A48923-001-a-0380">,</pc>
      <w lemma="Scotland" pos="nn1" rend="hi" xml:id="A48923-001-a-0390">Scotland</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-0400">and</w>
      <w lemma="Rome" pos="nn1" rend="hi" xml:id="A48923-001-a-0410">Rome</w>
      <pc xml:id="A48923-001-a-0420">;</pc>
     </l>
     <l xml:id="A48923-e130">
      <w lemma="yet" pos="av" xml:id="A48923-001-a-0430">Yet</w>
      <w lemma="both" pos="av-d" xml:id="A48923-001-a-0440">both</w>
      <w lemma="concentre" pos="vvb" xml:id="A48923-001-a-0450">Concentre</w>
      <w lemma="in" pos="acp" xml:id="A48923-001-a-0460">in</w>
      <w lemma="thy" pos="po" xml:id="A48923-001-a-0470">thy</w>
      <w lemma="doom" pos="n1" xml:id="A48923-001-a-0480">Doom</w>
      <pc unit="sentence" xml:id="A48923-001-a-0490">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e140">
     <l xml:id="A48923-e150">
      <w lemma="they" pos="pns" xml:id="A48923-001-a-0500">They</w>
      <w lemma="seem" pos="vvb" xml:id="A48923-001-a-0510">seem</w>
      <w lemma="each" pos="d" xml:id="A48923-001-a-0520">each</w>
      <w lemma="other" pos="pi-d" xml:id="A48923-001-a-0530">other</w>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-0540">to</w>
      <w lemma="engage" pos="vvi" xml:id="A48923-001-a-0550">Engage</w>
      <pc xml:id="A48923-001-a-0560">,</pc>
     </l>
     <l xml:id="A48923-e160">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-0570">And</w>
      <w lemma="bluster" pos="vvb" xml:id="A48923-001-a-0580">bluster</w>
      <w lemma="high" pos="j" xml:id="A48923-001-a-0590">high</w>
      <w lemma="upon" pos="acp" xml:id="A48923-001-a-0600">upon</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-0610">the</w>
      <w lemma="stage" pos="n1" xml:id="A48923-001-a-0620">Stage</w>
      <pc xml:id="A48923-001-a-0630">;</pc>
     </l>
     <l xml:id="A48923-e170">
      <w lemma="but" pos="acp" xml:id="A48923-001-a-0640">But</w>
      <w lemma="against" pos="acp" xml:id="A48923-001-a-0650">against</w>
      <w lemma="thou" pos="pno" xml:id="A48923-001-a-0660">thee</w>
      <w lemma="both" pos="d" xml:id="A48923-001-a-0670">both</w>
      <w lemma="bend" pos="vvb" xml:id="A48923-001-a-0680">bend</w>
      <w lemma="their" pos="po" xml:id="A48923-001-a-0690">their</w>
      <w lemma="rage" pos="n1" xml:id="A48923-001-a-0700">Rage</w>
      <pc unit="sentence" xml:id="A48923-001-a-0710">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e180">
     <l xml:id="A48923-e190">
      <w lemma="both" pos="av-d" xml:id="A48923-001-a-0720">Both</w>
      <w lemma="of" pos="acp" xml:id="A48923-001-a-0730">of</w>
      <w lemma="they" pos="pno" xml:id="A48923-001-a-0740">them</w>
      <w lemma="aim" pos="n1" xml:id="A48923-001-a-0750">Aim</w>
      <w lemma="at" pos="acp" xml:id="A48923-001-a-0760">at</w>
      <w lemma="thy" pos="po" xml:id="A48923-001-a-0770">thy</w>
      <w lemma="dear" pos="j" xml:id="A48923-001-a-0780">dear</w>
      <w lemma="life" pos="n1" xml:id="A48923-001-a-0790">Life</w>
      <pc xml:id="A48923-001-a-0800">,</pc>
     </l>
     <l xml:id="A48923-e200">
      <w lemma="but" pos="acp" xml:id="A48923-001-a-0810">But</w>
      <w lemma="whether" pos="cs" xml:id="A48923-001-a-0820">whether</w>
      <w lemma="rebellion" pos="n1" xml:id="A48923-001-a-0830">Rebellion</w>
      <pc xml:id="A48923-001-a-0840">,</pc>
      <w lemma="or" pos="cc" xml:id="A48923-001-a-0850">or</w>
      <w lemma="a" pos="d" xml:id="A48923-001-a-0860">a</w>
      <w lemma="knife" pos="n1" xml:id="A48923-001-a-0870">Knife</w>
     </l>
     <l xml:id="A48923-e210">
      <w lemma="shall" pos="vmb" xml:id="A48923-001-a-0880">Shall</w>
      <w join="right" lemma="do" pos="vvi" xml:id="A48923-001-a-0890">do</w>
      <w join="left" lemma="it" pos="pn" xml:id="A48923-001-a-0891">'t</w>
      <pc xml:id="A48923-001-a-0900">,</pc>
      <w lemma="be" pos="vvz" xml:id="A48923-001-a-0910">is</w>
      <w lemma="now" pos="av" xml:id="A48923-001-a-0920">now</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-0930">the</w>
      <w lemma="only" pos="j" xml:id="A48923-001-a-0940">only</w>
      <w lemma="strife" pos="n1" xml:id="A48923-001-a-0950">Strife</w>
      <pc unit="sentence" xml:id="A48923-001-a-0960">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e220">
     <l xml:id="A48923-e230">
      <w lemma="each" pos="d" xml:id="A48923-001-a-0970">Each</w>
      <w lemma="of" pos="acp" xml:id="A48923-001-a-0980">of</w>
      <w lemma="they" pos="pno" xml:id="A48923-001-a-0990">them</w>
      <w lemma="plot" pos="n2" xml:id="A48923-001-a-1000">Plots</w>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-1010">to</w>
      <w lemma="have" pos="vvi" xml:id="A48923-001-a-1020">have</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-1030">the</w>
      <w lemma="sway" pos="n1" xml:id="A48923-001-a-1040">Sway</w>
      <pc xml:id="A48923-001-a-1050">,</pc>
     </l>
     <l xml:id="A48923-e240">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-1060">And</w>
      <w lemma="struggle" pos="vvi" xml:id="A48923-001-a-1070">struggle</w>
      <w lemma="only" pos="av-j" xml:id="A48923-001-a-1080">only</w>
      <pc xml:id="A48923-001-a-1090">,</pc>
      <w lemma="that" pos="cs" xml:id="A48923-001-a-1100">that</w>
      <w lemma="it" pos="pn" xml:id="A48923-001-a-1110">it</w>
      <w lemma="may" pos="vmb" xml:id="A48923-001-a-1120">may</w>
     </l>
     <l xml:id="A48923-e250">
      <w lemma="be" pos="vvi" xml:id="A48923-001-a-1130">Be</w>
      <w lemma="bring" pos="vvn" xml:id="A48923-001-a-1140">brought</w>
      <w lemma="about" pos="acp" xml:id="A48923-001-a-1150">about</w>
      <w lemma="in" pos="acp" xml:id="A48923-001-a-1160">in</w>
      <w lemma="their" pos="po" xml:id="A48923-001-a-1170">their</w>
      <w lemma="own" pos="d" xml:id="A48923-001-a-1180">own</w>
      <w lemma="way" pos="n1" xml:id="A48923-001-a-1190">way</w>
      <pc unit="sentence" xml:id="A48923-001-a-1200">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e260">
     <l xml:id="A48923-e270">
      <w join="right" lemma="it" pos="pn" xml:id="A48923-001-a-1210">'T</w>
      <w join="left" lemma="be" pos="vvz" xml:id="A48923-001-a-1211">is</w>
      <w lemma="neither" pos="dx" xml:id="A48923-001-a-1220">neither</w>
      <w lemma="love" pos="n1" xml:id="A48923-001-a-1230">Love</w>
      <w lemma="nor" pos="ccx" xml:id="A48923-001-a-1240">nor</w>
      <w lemma="loyalty" pos="n1" xml:id="A48923-001-a-1250">Loyalty</w>
      <pc xml:id="A48923-001-a-1260">,</pc>
     </l>
     <l xml:id="A48923-e280">
      <w lemma="that" pos="cs" xml:id="A48923-001-a-1270">That</w>
      <w lemma="make" pos="vvb" xml:id="A48923-001-a-1280">make</w>
      <w lemma="fanatic" pos="n2-j" reg="fanatics" xml:id="A48923-001-a-1290">Phanaticks</w>
      <w lemma="talk" pos="vvb" xml:id="A48923-001-a-1300">talk</w>
      <w lemma="so" pos="av" xml:id="A48923-001-a-1310">so</w>
      <w lemma="high" pos="j" xml:id="A48923-001-a-1320">high</w>
     </l>
     <l xml:id="A48923-e290">
      <w lemma="against" pos="acp" xml:id="A48923-001-a-1330">'Gainst</w>
      <w lemma="popish" pos="j" xml:id="A48923-001-a-1350">Popish</w>
      <w lemma="plot" pos="n2" xml:id="A48923-001-a-1360">Plots</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-1370">and</w>
      <w lemma="treachery" pos="n1" xml:id="A48923-001-a-1380">Treachery</w>
      <pc unit="sentence" xml:id="A48923-001-a-1390">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e300">
     <l xml:id="A48923-e310">
      <w lemma="for" pos="acp" xml:id="A48923-001-a-1400">For</w>
      <w join="right" lemma="they" pos="pns" xml:id="A48923-001-a-1410">they</w>
      <w join="left" lemma="will" pos="vmb" reg="'ll" xml:id="A48923-001-a-1411">'l</w>
      <w lemma="rejoice" pos="vvi" reg="rejoice" xml:id="A48923-001-a-1420">rejoyce</w>
      <w lemma="at" pos="acp" xml:id="A48923-001-a-1430">at</w>
      <w lemma="charles" pos="nng1" reg="Charles'" rend="hi-apo-plain" xml:id="A48923-001-a-1440">Charles's</w>
      <w lemma="fall" pos="n1" xml:id="A48923-001-a-1460">Fall</w>
      <pc xml:id="A48923-001-a-1470">,</pc>
     </l>
     <l xml:id="A48923-e330">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-1480">And</w>
      <w lemma="hope" pos="vvb" xml:id="A48923-001-a-1490">hope</w>
      <pc xml:id="A48923-001-a-1500">,</pc>
      <w lemma="once" pos="acp" xml:id="A48923-001-a-1510">once</w>
      <w lemma="more" pos="avc-d" xml:id="A48923-001-a-1520">more</w>
      <pc xml:id="A48923-001-a-1530">,</pc>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-1540">to</w>
      <w lemma="have" pos="vvi" xml:id="A48923-001-a-1550">have</w>
      <w lemma="at" pos="acp" xml:id="A48923-001-a-1560">at</w>
      <w lemma="all" pos="d" xml:id="A48923-001-a-1570">all</w>
      <pc xml:id="A48923-001-a-1580">;</pc>
     </l>
     <l xml:id="A48923-e340">
      <w lemma="if" pos="cs" xml:id="A48923-001-a-1590">If</w>
      <w lemma="commonwealth" pos="n1" reg="Commonwealth" xml:id="A48923-001-a-1600">Common-Wealth</w>
      <w lemma="they" pos="pns" xml:id="A48923-001-a-1610">they</w>
      <w lemma="can" pos="vmd" xml:id="A48923-001-a-1620">could</w>
      <w lemma="recall" pos="vvi" reg="Recall" xml:id="A48923-001-a-1630">Recal</w>
      <pc unit="sentence" xml:id="A48923-001-a-1640">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e350">
     <l xml:id="A48923-e360">
      <w lemma="the" pos="d" xml:id="A48923-001-a-1650">The</w>
      <w lemma="papist" pos="nn2" xml:id="A48923-001-a-1660">Papists</w>
      <w lemma="hope" pos="n1" xml:id="A48923-001-a-1670">hope</w>
      <w lemma="will" pos="vmb" xml:id="A48923-001-a-1680">will</w>
      <w lemma="never" pos="avx" reg="ne'er" xml:id="A48923-001-a-1690">ne're</w>
      <w lemma="be" pos="vvi" xml:id="A48923-001-a-1700">be</w>
      <w lemma="go" pos="vvn" xml:id="A48923-001-a-1710">gone</w>
      <pc xml:id="A48923-001-a-1720">,</pc>
     </l>
     <l xml:id="A48923-e370">
      <w lemma="while" pos="cs" xml:id="A48923-001-a-1730">While</w>
      <w lemma="they" pos="pns" xml:id="A48923-001-a-1740">they</w>
      <w lemma="can" pos="vmb" xml:id="A48923-001-a-1750">can</w>
      <w lemma="set" pos="vvi" xml:id="A48923-001-a-1760">set</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-1770">the</w>
      <w lemma="faction" pos="n2" xml:id="A48923-001-a-1780">Factions</w>
      <w lemma="on" pos="acp" xml:id="A48923-001-a-1790">on</w>
      <pc xml:id="A48923-001-a-1800">,</pc>
     </l>
     <l xml:id="A48923-e380">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-1810">And</w>
      <w lemma="by" pos="acp" xml:id="A48923-001-a-1820">by</w>
      <w lemma="they" pos="pno" xml:id="A48923-001-a-1830">them</w>
      <w lemma="get" pos="vvi" xml:id="A48923-001-a-1840">get</w>
      <w lemma="their" pos="po" xml:id="A48923-001-a-1850">their</w>
      <w lemma="business" pos="n1" xml:id="A48923-001-a-1860">business</w>
      <w lemma="do" pos="vvn" xml:id="A48923-001-a-1870">done</w>
      <pc unit="sentence" xml:id="A48923-001-a-1880">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e390">
     <l xml:id="A48923-e400">
      <w lemma="the" pos="d" xml:id="A48923-001-a-1890">The</w>
      <w lemma="plotter" pos="n2" xml:id="A48923-001-a-1900">Plotters</w>
      <w lemma="thus" pos="av" xml:id="A48923-001-a-1910">thus</w>
      <w lemma="be" pos="vvb" xml:id="A48923-001-a-1920">are</w>
      <w lemma="leave" pos="vvn" xml:id="A48923-001-a-1930">left</w>
      <w lemma="untried" pos="j" reg="Untried" xml:id="A48923-001-a-1940">Untry'd</w>
      <pc xml:id="A48923-001-a-1950">,</pc>
     </l>
     <l xml:id="A48923-e410">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-1960">And</w>
      <w lemma="weighty" pos="js" xml:id="A48923-001-a-1970">weightiest</w>
      <w lemma="business" pos="n1" xml:id="A48923-001-a-1980">Business</w>
      <w lemma="lay" pos="vvn" xml:id="A48923-001-a-1990">laid</w>
      <w lemma="aside" pos="av" xml:id="A48923-001-a-2000">aside</w>
      <pc xml:id="A48923-001-a-2010">,</pc>
     </l>
     <l xml:id="A48923-e420">
      <w lemma="till" pos="acp" xml:id="A48923-001-a-2020">Till</w>
      <w lemma="private" pos="j" xml:id="A48923-001-a-2030">private</w>
      <w lemma="rage" pos="n1" xml:id="A48923-001-a-2040">Rage</w>
      <w lemma="be" pos="vvi" xml:id="A48923-001-a-2050">be</w>
      <w lemma="satisfy" pos="vvn" reg="satisfied" xml:id="A48923-001-a-2060">satisfy'd</w>
      <pc unit="sentence" xml:id="A48923-001-a-2070">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e430">
     <l xml:id="A48923-e440">
      <w lemma="our" pos="po" xml:id="A48923-001-a-2080">Our</w>
      <w lemma="prince" pos="ng1" reg="Prince's" xml:id="A48923-001-a-2090">Princes</w>
      <w lemma="friend" pos="n2" xml:id="A48923-001-a-2100">Friends</w>
      <w lemma="we" pos="pns" xml:id="A48923-001-a-2110">we</w>
      <w lemma="first" pos="ord" xml:id="A48923-001-a-2120">first</w>
      <w lemma="pursue" pos="vvi" xml:id="A48923-001-a-2130">pursue</w>
      <pc xml:id="A48923-001-a-2140">,</pc>
     </l>
     <l xml:id="A48923-e450">
      <w lemma="who" pos="crq" xml:id="A48923-001-a-2150">Whom</w>
      <w lemma="we" pos="pns" xml:id="A48923-001-a-2160">we</w>
      <w lemma="count" pos="vvb" xml:id="A48923-001-a-2170">count</w>
      <w lemma="false" pos="j" xml:id="A48923-001-a-2180">False</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-2190">and</w>
      <w lemma="he" pos="pns" xml:id="A48923-001-a-2200">he</w>
      <w lemma="count" pos="vvz" xml:id="A48923-001-a-2210">counts</w>
      <w lemma="true" pos="j" xml:id="A48923-001-a-2220">True</w>
      <pc xml:id="A48923-001-a-2230">,</pc>
     </l>
     <l xml:id="A48923-e460">
      <w lemma="ere" pos="acp" reg="ere" xml:id="A48923-001-a-2240">E're</w>
      <w lemma="his" pos="po" xml:id="A48923-001-a-2250">his</w>
      <w lemma="own" pos="d" xml:id="A48923-001-a-2260">own</w>
      <w lemma="foe" pos="n2" xml:id="A48923-001-a-2270">Foes</w>
      <w lemma="can" pos="vmb" xml:id="A48923-001-a-2280">can</w>
      <w lemma="have" pos="vvi" xml:id="A48923-001-a-2290">have</w>
      <w lemma="their" pos="po" xml:id="A48923-001-a-2300">their</w>
      <w lemma="due" pos="n1-j" xml:id="A48923-001-a-2310">due</w>
      <pc unit="sentence" xml:id="A48923-001-a-2320">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e470">
     <l xml:id="A48923-e480">
      <w lemma="the" pos="d" xml:id="A48923-001-a-2330">The</w>
      <w lemma="tawny" pos="j" xml:id="A48923-001-a-2340">Tawny</w>
      <w lemma="turncoat" pos="n1" xml:id="A48923-001-a-2350">Turncoat</w>
      <w lemma="do" pos="vvz" xml:id="A48923-001-a-2360">doth</w>
      <w lemma="suggest" pos="vvi" xml:id="A48923-001-a-2370">suggest</w>
      <pc xml:id="A48923-001-a-2380">,</pc>
     </l>
     <l xml:id="A48923-e490">
      <w lemma="the" pos="d" xml:id="A48923-001-a-2390">The</w>
      <w lemma="bishop" pos="n2" xml:id="A48923-001-a-2400">Bishops</w>
      <w lemma="too" pos="av" xml:id="A48923-001-a-2410">too</w>
      <pc xml:id="A48923-001-a-2420">,</pc>
      <w lemma="among" pos="acp" xml:id="A48923-001-a-2430">amongst</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-2440">the</w>
      <w lemma="rest" pos="n1" xml:id="A48923-001-a-2450">rest</w>
      <pc xml:id="A48923-001-a-2460">,</pc>
     </l>
     <l xml:id="A48923-e500">
      <w lemma="be" pos="vvb" xml:id="A48923-001-a-2470">Are</w>
      <w lemma="plotter" pos="n2" xml:id="A48923-001-a-2480">Plotters</w>
      <pc xml:id="A48923-001-a-2490">,</pc>
      <w lemma="though" pos="cs" xml:id="A48923-001-a-2500">though</w>
      <w lemma="they" pos="pns" xml:id="A48923-001-a-2510">they</w>
      <w lemma="take" pos="vvb" xml:id="A48923-001-a-2520">take</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-2530">the</w>
      <w lemma="test" pos="n1" xml:id="A48923-001-a-2540">Test</w>
      <pc unit="sentence" xml:id="A48923-001-a-2550">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e510">
     <l xml:id="A48923-e520">
      <w lemma="yea" pos="uh" xml:id="A48923-001-a-2560">Yea</w>
      <pc xml:id="A48923-001-a-2570">,</pc>
      <w lemma="he" pos="pns" xml:id="A48923-001-a-2580">He</w>
      <w lemma="assure" pos="vvz" xml:id="A48923-001-a-2590">assures</w>
      <w lemma="we" pos="pno" xml:id="A48923-001-a-2600">us</w>
      <w lemma="there" pos="av" xml:id="A48923-001-a-2610">there</w>
      <w lemma="be" pos="vvb" xml:id="A48923-001-a-2620">are</w>
      <w lemma="fear" pos="n2" xml:id="A48923-001-a-2630">Fears</w>
      <pc xml:id="A48923-001-a-2640">,</pc>
     </l>
     <l xml:id="A48923-e530">
      <w lemma="that" pos="cs" xml:id="A48923-001-a-2650">That</w>
      <w lemma="all" pos="d" xml:id="A48923-001-a-2660">all</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-2670">the</w>
      <w lemma="old" pos="j" xml:id="A48923-001-a-2680">old</w>
      <w lemma="great" pos="j" xml:id="A48923-001-a-2690">great</w>
      <w lemma="cavalier" pos="n2" xml:id="A48923-001-a-2700">Cavaliers</w>
     </l>
     <l xml:id="A48923-e540">
      <w lemma="be" pos="vvb" xml:id="A48923-001-a-2710">Are</w>
      <w lemma="in" pos="acp" xml:id="A48923-001-a-2720">in</w>
      <w lemma="it" pos="pn" xml:id="A48923-001-a-2730">it</w>
      <pc xml:id="A48923-001-a-2740">,</pc>
      <w lemma="over" pos="acp" xml:id="A48923-001-a-2750">over</w>
      <w lemma="head" pos="n1" xml:id="A48923-001-a-2760">Head</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-2770">and</w>
      <w lemma="ear" pos="n2" xml:id="A48923-001-a-2780">Ears</w>
      <pc unit="sentence" xml:id="A48923-001-a-2790">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e550">
     <l xml:id="A48923-e560">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-2800">And</w>
      <w lemma="some" pos="d" xml:id="A48923-001-a-2810">some</w>
      <w lemma="there" pos="av" xml:id="A48923-001-a-2820">there</w>
      <w lemma="be" pos="vvb" xml:id="A48923-001-a-2830">are</w>
      <w lemma="that" pos="cs" xml:id="A48923-001-a-2840">that</w>
      <w lemma="grave" pos="av-j" xml:id="A48923-001-a-2850">gravely</w>
      <w lemma="say" pos="vvb" xml:id="A48923-001-a-2860">say</w>
      <pc xml:id="A48923-001-a-2870">,</pc>
     </l>
     <l xml:id="A48923-e570">
      <w lemma="the" pos="d" xml:id="A48923-001-a-2880">The</w>
      <w lemma="king" pos="n1" xml:id="A48923-001-a-2890">King</w>
      <w lemma="do" pos="vvd" xml:id="A48923-001-a-2900">did</w>
      <w lemma="help" pos="vvi" xml:id="A48923-001-a-2910">help</w>
      <w lemma="this" pos="d" xml:id="A48923-001-a-2920">this</w>
      <w lemma="plot" pos="n1" xml:id="A48923-001-a-2930">Plot</w>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-2940">to</w>
      <w lemma="lay" pos="vvi" xml:id="A48923-001-a-2950">lay</w>
      <pc xml:id="A48923-001-a-2960">,</pc>
     </l>
     <l xml:id="A48923-e580">
      <w lemma="for" pos="acp" xml:id="A48923-001-a-2970">For</w>
      <w lemma="take" pos="vvg" xml:id="A48923-001-a-2980">taking</w>
      <w lemma="his" pos="po" xml:id="A48923-001-a-2990">his</w>
      <w lemma="own" pos="d" xml:id="A48923-001-a-3000">own</w>
      <w lemma="life" pos="n1" xml:id="A48923-001-a-3010">life</w>
      <w lemma="away" pos="av" xml:id="A48923-001-a-3020">away</w>
      <pc unit="sentence" xml:id="A48923-001-a-3030">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e590">
     <l xml:id="A48923-e600">
      <w lemma="and" pos="cc" xml:id="A48923-001-a-3040">And</w>
      <w lemma="thus" pos="av" xml:id="A48923-001-a-3050">thus</w>
      <pc xml:id="A48923-001-a-3060">,</pc>
      <w lemma="under" pos="acp" xml:id="A48923-001-a-3070">under</w>
      <w lemma="pretence" pos="n1" xml:id="A48923-001-a-3080">pretence</w>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-3090">to</w>
      <w lemma="sift" pos="vvb" xml:id="A48923-001-a-3100">Sift</w>
     </l>
     <l xml:id="A48923-e610">
      <w lemma="the" pos="d" xml:id="A48923-001-a-3110">The</w>
      <w lemma="plot" pos="n1" xml:id="A48923-001-a-3120">Plot</w>
      <w lemma="to" pos="acp" xml:id="A48923-001-a-3130">to</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-3140">the</w>
      <w lemma="bottom" pos="n1" xml:id="A48923-001-a-3150">bottom</w>
      <pc xml:id="A48923-001-a-3160">;</pc>
      <w lemma="their" pos="po" xml:id="A48923-001-a-3170">their</w>
      <w lemma="main" pos="j" xml:id="A48923-001-a-3180">main</w>
      <w lemma="drift" pos="n1" xml:id="A48923-001-a-3190">drift</w>
     </l>
     <l xml:id="A48923-e620">
      <w lemma="be" pos="vvz" xml:id="A48923-001-a-3200">Is</w>
      <w lemma="at" pos="acp" xml:id="A48923-001-a-3210">at</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-3220">the</w>
      <w lemma="government" pos="n1" xml:id="A48923-001-a-3230">Government</w>
      <w lemma="to" pos="prt" xml:id="A48923-001-a-3240">to</w>
      <w lemma="lift" pos="vvi" xml:id="A48923-001-a-3250">lift</w>
      <pc unit="sentence" xml:id="A48923-001-a-3260">.</pc>
     </l>
    </lg>
    <lg xml:id="A48923-e630">
     <l xml:id="A48923-e640">
      <w lemma="nor" pos="ccx" xml:id="A48923-001-a-3270">Nor</w>
      <w lemma="will" pos="vmb" xml:id="A48923-001-a-3280">will</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-3290">the</w>
      <w lemma="plot" pos="n1" xml:id="A48923-001-a-3300">Plot</w>
      <w lemma="serve" pos="vvi" xml:id="A48923-001-a-3310">serve</w>
      <w lemma="their" pos="po" xml:id="A48923-001-a-3320">their</w>
      <w lemma="base" pos="j" xml:id="A48923-001-a-3330">base</w>
      <w lemma="end" pos="n2" xml:id="A48923-001-a-3340">Ends</w>
      <pc xml:id="A48923-001-a-3350">,</pc>
     </l>
     <l xml:id="A48923-e650">
      <w lemma="unless" pos="cs" xml:id="A48923-001-a-3360">Unless</w>
      <w lemma="it" pos="pn" xml:id="A48923-001-a-3370">it</w>
      <w lemma="to" pos="acp" xml:id="A48923-001-a-3380">to</w>
      <w lemma="the" pos="d" xml:id="A48923-001-a-3390">the</w>
      <w lemma="ruin" pos="n1" reg="Ruin" xml:id="A48923-001-a-3400">Ruine</w>
      <w lemma="bend" pos="vvz" xml:id="A48923-001-a-3410">bends</w>
     </l>
     <l xml:id="A48923-e660">
      <w lemma="of" pos="acp" xml:id="A48923-001-a-3420">Of</w>
      <w lemma="monarchy" pos="n1" xml:id="A48923-001-a-3430">Monarchy</w>
      <w lemma="and" pos="cc" xml:id="A48923-001-a-3440">and</w>
      <w lemma="all" pos="d" xml:id="A48923-001-a-3450">all</w>
      <w lemma="its" pos="po" xml:id="A48923-001-a-3460">its</w>
      <w lemma="friend" pos="n2" xml:id="A48923-001-a-3470">Friends</w>
      <pc unit="sentence" xml:id="A48923-001-a-3480">.</pc>
     </l>
    </lg>
   </div>
  </body>
  <back xml:id="A48923-e670">
   <div type="colophon" xml:id="A48923-e680">
    <p xml:id="A48923-e690">
     <w lemma="LONDON" pos="nn1" xml:id="A48923-001-a-3490">LONDON</w>
     <pc xml:id="A48923-001-a-3500">,</pc>
     <hi xml:id="A48923-e700">
      <w lemma="print" pos="vvn" xml:id="A48923-001-a-3510">Printed</w>
      <w lemma="for" pos="acp" xml:id="A48923-001-a-3520">for</w>
     </hi>
     <w lemma="t." pos="ab" xml:id="A48923-001-a-3530">T.</w>
     <w lemma="d." pos="ab" xml:id="A48923-001-a-3540">D.</w>
     <w lemma="1679." pos="crd" xml:id="A48923-001-a-3550">1679.</w>
     <pc unit="sentence" xml:id="A48923-001-a-3560"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
